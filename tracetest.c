#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char **argv)
{
	int fd;

  	//turn on tracing
	trace(1);
	fd = open("file.c", 0);
	getpid();
	close(fd);

	//turn off tracing
	trace(0);
	fd = open("file.c", 0);
	getpid();
	close(fd);

	return 0;
}
